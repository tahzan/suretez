from suretez.core.utils import FilenameGenerator
from django.db import models


class Settings(models.Model):
    TYPE = (
        ('about_us_title', 'about_us_title'),
        ('about_us_caption', 'about_us_caption'),
        ('about_us_banner', 'about_us_banner'),
        ('about_us_description', 'about_us_description'),
        ('about_us_long_description', 'about_us_long_description'),

        # ('benefit_title', 'benefit_title'),
        # ('benefit_1_img', 'benefit_1_img'),
        # ('benefit_1_desc', 'benefit_1_desc'),
        # ('benefit_2_img', 'benefit_2_img'),
        # ('benefit_2_desc', 'benefit_2_desc'),
        # ('benefit_3_img', 'benefit_3_img'),
        # ('benefit_3_desc', 'benefit_3_desc'),

        ('blog_banner', 'blog_banner'),
        ('blog_desc', 'blog_desc'),
        ('blog_title', 'blog_title'),

        ('favicon', 'favicon'),

        ('find_product_title', 'find_product_title'),
        ('find_product_img1', 'find_product_img1'),
        ('find_product_img2', 'find_product_img2'),
        ('find_product_img3', 'find_product_img3'),
        ('find_product_img4', 'find_product_img4'),


        ('footer_logo', 'footer_logo'),
        ('footer_address_left', 'footer_address_left'),
        ('footer_bottom_text', 'footer_bottom_text'),

        ('invite_title', 'invite_title'),
        ('invite_description', 'invite_description'),
        ('invite_background', 'invite_background'),

        # ('invite_title_2', 'invite_title_2'),
        # ('invite_description_2', 'invite_description_2'),
        # ('invite_background_2', 'invite_background_2'),

        ('logo', 'logo'),

        # ('product_left_img', 'product_left_img'),
        # ('product_description', 'product_description'),
        # ('product_text_img', 'product_text_img'),
        
        
        # ('product_1_title', 'product_1_title'),
        # ('product_1_banner', 'product_1_banner'),
        # ('product_1_description', 'product_1_description'),
        # ('product_img_1', 'product_img_1'),
        # ('product_img_2', 'product_img_2'),
        # ('product_img_3', 'product_img_3'),
        # ('product_img_4', 'product_img_4'),

        # ('product_2_title', 'product_2_title'),
        # ('product_2_banner', 'product_2_banner'),
        # ('product_2_description', 'product_1_description'),
        # ('product_img_5', 'product_img_5'),
        # ('product_img_6', 'product_img_6'),
        # ('product_img_7', 'product_img_7'),
        # ('product_img_8', 'product_img_8'),
        ('products_banner', 'products_banner'),
        ('product_view_more', 'product_view_more'),

        ('seo_description', 'SEO Description'),
        ('seo_title', 'SEO Title'),
        ('seo_keywords', 'SEO Keywords'),
        ('seo_image', 'SEO Image'),

        ('testimoni_title', 'testimoni_title'),

        # socmed section
        ('socmed_big_img', 'socmed_big_img'),
        ('youtube_url', 'youtube_url' ),
        ('instagram_url', 'instagram_url' ),
        ('facebook_url', 'facebook_url' ),
        ('youtube_img', 'youtube_img' ),
        ('instagram_img', 'instagram_img' ),
        ('facebook_img', 'facebook_img' )
    )
    name = models.CharField(max_length=254, choices=TYPE, unique=True)
    text_value = models.TextField(blank=True, null=True)
    img_value =  models.ImageField(upload_to=FilenameGenerator(
        prefix='settings'), blank=True, null=True)

    def __str__(self):
        return self.name
