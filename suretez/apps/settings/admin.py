from django.contrib import admin
from django_summernote.admin import SummernoteModelAdmin

from .models import *


class SettingsModelAdmin(SummernoteModelAdmin):  # instead of ModelAdmin
    ordering = ('name',)
    list_display = ('name', 'text_value', 'img_value')
    summernote_fields = '__all__'

admin.site.register(Settings, SettingsModelAdmin)


from django_summernote.utils import get_attachment_model 
admin.site.unregister(get_attachment_model())
