# Generated by Django 3.1.2 on 2021-03-17 01:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('settings', '0007_auto_20210228_1210'),
    ]

    operations = [
        migrations.AlterField(
            model_name='settings',
            name='name',
            field=models.CharField(choices=[('about_us_description', 'about_us_description'), ('about_us_title', 'about_us_title'), ('about_us_caption', 'about_us_caption'), ('benefit_title', 'benefit_title'), ('benefit_1_img', 'benefit_1_img'), ('benefit_1_desc', 'benefit_1_desc'), ('benefit_2_img', 'benefit_2_img'), ('benefit_2_desc', 'benefit_2_desc'), ('benefit_3_img', 'benefit_3_img'), ('benefit_3_desc', 'benefit_3_desc'), ('blog_banner', 'blog_banner'), ('blog_desc', 'blog_desc'), ('blog_title', 'blog_title'), ('facebook_url', 'facebook_url'), ('favicon', 'favicon'), ('find_product_title', 'find_product_title'), ('find_product_img1', 'find_product_img1'), ('find_product_img2', 'find_product_img2'), ('find_product_img3', 'find_product_img3'), ('find_product_img4', 'find_product_img4'), ('find_product_img5', 'find_product_img5'), ('find_product_img6', 'find_product_img6'), ('find_product_img7', 'find_product_img7'), ('find_product_img8', 'find_product_img8'), ('footer_logo', 'footer_logo'), ('footer_address_left', 'footer_address_left'), ('footer_address_right', 'footer_address_right'), ('footer_bottom_text', 'footer_bottom_text'), ('invite_title', 'invite_title'), ('invite_description', 'invite_description'), ('invite_background', 'invite_background'), ('invite_title_2', 'invite_title_2'), ('invite_description_2', 'invite_description_2'), ('invite_background_2', 'invite_background_2'), ('logo', 'logo'), ('product_left_img', 'product_left_img'), ('product_description', 'product_description'), ('product_text_img', 'product_text_img'), ('product_name', 'product_name'), ('product_img_1', 'product_img_1'), ('product_img_2', 'product_img_2'), ('product_img_3', 'product_img_3'), ('product_img_4', 'product_img_4'), ('seo_description', 'SEO Description'), ('seo_title', 'SEO Title'), ('seo_keywords', 'SEO Keywords'), ('seo_image', 'SEO Image'), ('testimoni_title', 'testimoni_title'), ('linkedin_url', 'linkedin_url'), ('twitter_url', 'twitter_url'), ('google_url', 'google_url'), ('instagram_url', 'instagram_url'), ('facebook_url', 'facebook_url')], max_length=254, unique=True),
        ),
    ]
