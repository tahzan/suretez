from suretez.core.utils import FilenameGenerator
from django.db import models


class Testimoni(models.Model):
    testimony = models.TextField()
    user = models.CharField(max_length=50, null=True)
    address = models.CharField(max_length=50, null=True)
    img =  models.ImageField(upload_to=FilenameGenerator(
        prefix='testimony'), blank=True, null=True)
    is_active = models.BooleanField(default=True)
    
    def __str__(self):
        return self.testimony