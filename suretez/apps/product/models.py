from suretez.core.utils import FilenameGenerator, custom_slugify
from django.db import models
from django.utils.safestring import mark_safe


class Product(models.Model):
    name = models.CharField("Name", max_length=255)
    banner =  models.ImageField("banner 10:4", upload_to=FilenameGenerator(
        prefix='product'), null=True)
    slug = models.CharField(unique=True, blank=True, max_length=254)
    is_active = models.BooleanField("Is Active", default=True)
    desc = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = custom_slugify(self.name)
        super(Product, self).save()


class ProductPhoto(models.Model):
    img =  models.ImageField("image", upload_to=FilenameGenerator(
        prefix='photo'))
    product = models.ForeignKey('product.Product', on_delete=models.CASCADE)
    
    def __str__(self):
        return f'{self.product.name}-{self.id}' 