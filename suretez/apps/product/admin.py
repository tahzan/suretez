from django.contrib import admin
from .models import *
from django_summernote.admin import SummernoteModelAdmin


class ProductLine(admin.TabularInline):
    model = ProductPhoto


class ProductAdmin(SummernoteModelAdmin):
    inlines = (ProductLine,)

admin.site.register(Product, ProductAdmin)
