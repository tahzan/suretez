# Generated by Django 3.1.2 on 2021-03-17 01:17

from django.db import migrations, models
import suretez.core.utils


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20201029_0612'),
    ]

    operations = [
        migrations.AddField(
            model_name='blog',
            name='img2',
            field=models.ImageField(null=True, upload_to=suretez.core.utils.FilenameGenerator(prefix='banner'), verbose_name='img 10:5'),
        ),
        migrations.AlterField(
            model_name='blog',
            name='img1',
            field=models.ImageField(upload_to=suretez.core.utils.FilenameGenerator(prefix='banner'), verbose_name='img 3:2'),
        ),
    ]
