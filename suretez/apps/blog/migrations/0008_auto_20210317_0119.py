# Generated by Django 3.1.2 on 2021-03-17 01:19

from django.db import migrations, models
import suretez.core.utils


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0007_auto_20210317_0117'),
    ]

    operations = [
        migrations.AlterField(
            model_name='blog',
            name='img2',
            field=models.ImageField(null=True, upload_to=suretez.core.utils.FilenameGenerator(prefix='banner'), verbose_name='img 10:4'),
        ),
    ]
