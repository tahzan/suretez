from django.http import JsonResponse
from django.template.response import TemplateResponse
from django.template import loader
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


from suretez.apps.banner.models import Banner
from suretez.apps.blog.models import Blog
from suretez.apps.menu.models import Menu
from suretez.apps.portfolio.models import Portfolio
from suretez.apps.pricing.models import Pricing
from suretez.apps.service.models import Service
from suretez.apps.settings.models import Settings
from suretez.apps.team.models import Team
from suretez.apps.testimoni.models import Testimoni
from suretez.apps.product.models import Product
from suretez.core.serializer import serialize_settings

from .forms import ContactusForm


def index(request):
    settings = Settings.objects.all()
    serialize_settings_dict={}
    for setting in settings:
        serialize_settings_dict[setting.name] = serialize_settings(setting)    
    form = ContactusForm(data=request.POST or None)
    menus = Menu.objects.filter(is_active=True).order_by('order')
    products = Product.objects.filter(is_active=True).all()
    context = {
        "banners" : Banner.objects.filter(is_active=True).order_by('-id'),
        "blogs" : Blog.objects.filter(is_active=True).order_by('-id')[:3],
        "menus" : menus,
        "menu_list" : [menu.one_page_url for menu in menus],
        "portfolios" : Portfolio.objects.filter(is_active=True).order_by('id'),
        "pricings" : Pricing.objects.filter(is_active=True).order_by('-id'),
        "services" : Service.objects.filter(is_active=True).order_by('-id'),
        "teams" : Team.objects.filter(is_active=True).order_by('-id'),
        "testimonies" : Testimoni.objects.filter(is_active=True).order_by('id'),
        "settings" : serialize_settings_dict,
        "products": products,
        "form": form,
        "homepage": True
    }
    return TemplateResponse(request, 'frontend/index.html', context)

def contactus(request):
    form = ContactusForm(data=request.POST or None)
    if form.is_valid():
        form.save()
    data={"success": "true", "message":""}
    return JsonResponse(data)


def blog(request):
    settings = Settings.objects.all()
    serialize_settings_dict={}
    for setting in settings:
        serialize_settings_dict[setting.name] = serialize_settings(setting)

    blogs = Blog.objects.filter(is_active=True).order_by('-id')[:3]
    menus = Menu.objects.filter(is_active=True).order_by('order')
    return TemplateResponse(request, 'frontend/blog-list.html', {
        "blogs": blogs, 
        "menus" : menus,
        "menu_list" : [menu.one_page_url for menu in menus],
        "settings" : serialize_settings_dict,
        "homepage": False
    })


def about_us(request):
    settings = Settings.objects.all()
    serialize_settings_dict={}
    for setting in settings:
        serialize_settings_dict[setting.name] = serialize_settings(setting)
    menus = Menu.objects.filter(is_active=True).order_by('order')
    return TemplateResponse(request, 'frontend/about-us.html', {
        "menus" : menus,
        "menu_list" : [menu.one_page_url for menu in menus],
        "settings" : serialize_settings_dict,
        "homepage": False,
    })


def products(request):
    settings = Settings.objects.all()
    serialize_settings_dict={}
    for setting in settings:
        serialize_settings_dict[setting.name] = serialize_settings(setting)
    menus = Menu.objects.filter(is_active=True).order_by('order')
    products = Product.objects.filter(is_active=True).all()
    return TemplateResponse(request, 'frontend/product.html', {
        "menus" : menus,
        "menu_list" : [menu.one_page_url for menu in menus],
        "settings" : serialize_settings_dict,
        "homepage": False,
        "products": products,
    })


def blogdetail(request, slug):
    settings = Settings.objects.all()
    serialize_settings_dict={}
    for setting in settings:
        serialize_settings_dict[setting.name] = serialize_settings(setting)

    blog = Blog.objects.get(slug=slug)
    blogs = Blog.objects.filter(is_active=True)\
        .exclude(slug=slug).order_by('-id')[:3]
    menus = Menu.objects.filter(is_active=True).order_by('order')
    return TemplateResponse(request, 'frontend/blog-detail.html', {
        "blog": blog, 
        "blogs": blogs, 
        "menus" : menus,
        "menu_list" : [menu.one_page_url for menu in menus],
        "settings" : serialize_settings_dict,
        "homepage": False
    })


def product_detail(request, slug):
    settings = Settings.objects.all()
    serialize_settings_dict={}
    for setting in settings:
        serialize_settings_dict[setting.name] = serialize_settings(setting)

    product = Product.objects.get(slug=slug)
    menus = Menu.objects.filter(is_active=True).order_by('order')
    return TemplateResponse(request, 'frontend/product-detail.html', {
        "product": product, 
        "menus" : menus,
        "menu_list" : [menu.one_page_url for menu in menus],
        "settings" : serialize_settings_dict,
        "homepage": False
    })


def lazy_load_blogs(request):
    page = request.POST.get('page')
    blogs = Blog.objects.filter(is_active=True).all().order_by('-id')
    results_per_page = 3
    paginator = Paginator(blogs, results_per_page)
    try:
        blogs = paginator.page(page)
    except PageNotAnInteger:
        blogs = paginator.page(2)
    except EmptyPage:
        blogs = paginator.page(paginator.num_pages)

    # build a html blogs list with the paginated blogs
    blogs_html = loader.render_to_string(
        'frontend/blogs.html',
        {'blogs': blogs}
    )
    output_data = {
        'blogs_html': blogs_html,
        'has_next': blogs.has_next()
    }
    return JsonResponse(output_data)
